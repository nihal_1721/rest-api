from flask import Flask
import sqlite3
import json
app = Flask(__name__)


@app.route('/api/getallstudentdata', methods=['GET'])
def getallstudent_details():
	conn = sqlite3.connect('stud.db')
	print ('Opened Database Succesfully')
	abc = []
	cursor = conn.execute("SELECT * from student")
	for row in cursor:
		xyz = {
		'Id' : row[0],
		'Firstname' : row[1],
		'Lastname' : row[2],
		'Phonenumber' : row[3]
		}
		abc.append(xyz)
	abc = str(abc)
	return abc

@app.route('/api/getstudentdata/<int:stud_id>', methods=['GET'])
def getstudent_details(stud_id):
	conn = sqlite3.connect('stud.db')
	print ('Opened Database Succesfully')
	abc = dict()
	cursor = conn.execute("SELECT id, firstname, lastname, phonenumber from student")
	for row in cursor:
		if row[0] == stud_id:
			abc['id']=row[0]
			abc['Firstname']=row[1]
			abc['Lastname']=row[2]
			abc['Phonenumber']=row[3]
	return abc

@app.route('/api/insertstudentdata/<id>/<firstname>/<lastname>/<phonenumber>', methods=['POST'])
def insertstudent_details(id,firstname,lastname,phonenumber):
	conn = sqlite3.connect('stud.db')
	print ('Opened Database Succesfully')
	ins = conn.execute("INSERT into STUDENT (id,firstname,lastname,phonenumber) values(id,firstname,lastname,phonenumber)")
	return 'testing Inserted Student Data'

